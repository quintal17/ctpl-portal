"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
;
var ValidatorService = /** @class */ (function () {
    function ValidatorService() {
    }
    ValidatorService.prototype.validateClient = function (cli) {
        console.log(Object.getOwnPropertyNames(cli));
        var head = "Please fill up the following required fields: <br/>";
        var body = "";
        this.isNotNull(cli.fName) ? null : body += '<p>*First Name' + "</p>";
        this.isNotNull(cli.lName) ? null : body += '<p>*Last Name' + "</p>";
        this.isNotNull(cli.birthdate) ? null : body += '<p>*Birth Date' + "</p>";
        this.isNotNull(cli.province) ? null : body += '<p>*Province' + "</p>";
        this.isNotNull(cli.municipality) ? null : body += '<p>*Municipality' + "</p>";
        this.isNotNull(cli.zipCode) ? null : body += '<p>*Zip Code' + "</p>";
        this.isNotNull(cli.address1) ? null : body += '<p>*Address' + "</p>";
        this.isNotNull(cli.address2) ? null : body += '<p>*Address' + "</p>";
        this.isNotNull(cli.address3) ? null : body += '<p>*Address' + "</p>";
        this.isNotNull(cli.codDoc) ? null : body += '<p>*Document Code' + "</p>";
        this.isNotNull(cli.tipDoc) ? null : body += '<p>*Document Type' + "</p>";
        console.log(cli.birthdate + "birthdate");
        return this.isNotNull(body) ? head + body : "";
        // fName: string;
        // mName: string;
        // lName: string;
        // province: string;
        // municipality: string;
        // address1: string;
        // address2: string;
        // address3: string;
        // codDoc: string;
        // tipDoc: string;
        // zipCode: string;
        // suffix : string;
        // mobileNumber: string;
        // email: string;
        // birthdate: string;
    };
    ValidatorService.prototype.validateVehicle = function (veh) {
        console.log(Object.getOwnPropertyNames(cli));
        var head = "Please fill up the following required fields: <br/>";
        var body = "";
        this.isNotNull(veh.make) ? null : body += '<p>*First Name' + "</p>";
        this.isNotNull(veh.model) ? null : body += '<p>*Last Name' + "</p>";
        this.isNotNull(veh.) ? null : body += '<p>*Birth Date' + "</p>";
        this.isNotNull(veh.province) ? null : body += '<p>*Province' + "</p>";
        this.isNotNull(veh.municipality) ? null : body += '<p>*Municipality' + "</p>";
        this.isNotNull(veh.zipCode) ? null : body += '<p>*Zip Code' + "</p>";
        this.isNotNull(veh.address1) ? null : body += '<p>*Address' + "</p>";
        this.isNotNull(veh.address2) ? null : body += '<p>*Address' + "</p>";
        this.isNotNull(veh.address3) ? null : body += '<p>*Address' + "</p>";
        this.isNotNull(veh.codDoc) ? null : body += '<p>*Document Code' + "</p>";
        this.isNotNull(veh.tipDoc) ? null : body += '<p>*Document Type' + "</p>";
        return this.isNotNull(body) ? head + body : "";
        //  make: string;
        // model: string;
        // variant: string;
        // mvNumber: string;
        // plateNumber: string;
        // engineNumber: string;
        // chassisNumber: string;
        // inceptionDate: string;
        // expiryDate: string;
        // color: string;
        // year: string;
        // subModel: string;
        // typeOfUse: string;
        // businessLine: string;
        // numPoliza: string;
        // zipCode: string;
        // tipCocafRegistration:string;
        // txtMotivoSpto:string;
        // mvNumber1st: string;
        // mvNumber2nd: string;
    };
    ValidatorService.prototype.nxtLine = function (body) {
        return this.isNotNull(body) ? '<br/>' : '';
        ;
    };
    ValidatorService.prototype.isNotNull = function (str) {
        return !(str == null || str == undefined || str == "");
    };
    ValidatorService = __decorate([
        core_1.Injectable()
    ], ValidatorService);
    return ValidatorService;
}());
exports.ValidatorService = ValidatorService;
//# sourceMappingURL=validator.component.js.map