import { Component, OnInit } from '@angular/core';
import {ModalService} from './../_services/modal.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  constructor(private modal : ModalService,
    private scroll: ScrollToService) { }

  ngOnInit() {
  }

  showReminder(){
  	var reminderMessageg = '<p> <strong>Fill out the required fields with the correct vehicle and owner information.</strong></p>'+
							'<li>NOTE: Take extra care in filling out the following: MV File Number, Serial/Chassis Number, Plate Number. Incorrect information will leave your application unapproved.</li>'+
							'<br>'+
							'<p> <strong>Fill out the required fields with the correct vehicle and owner information.</strong></p>'+
							'<li>For non-brand new units or renewal registration,plate number should be updated in LTO database </li>'+
							'<li>For brand new units or first registration, details Certificate of Stock Reported or CSR particularly motor number and serial number should be updated in LTO database.</li>'+
							'<li>Call the LTO Branch where you registered your Plate Number then ask for an update on their system and confirm.</li>'+
							'<br>'+
							'<p><strong>Check that your vehicle is due for registration.</strong> (At least 3 months before the expiry of the previous registration)</p>'+
							'<li>If you have already paid for your CTPL application and your vehicle is not yet due for registration, your application will be on queue until the date of your due. Until then, your Certificate of Cover will not be issued.'+
							    '<br>'+
							    '<br>'+
							    '<ol>In accordance with the Administrative Order No. 010-2005 of the Department of Transportation and Communications dated January 3, 2005, the initial registration of all brand new motor vehicles, shall be valid and effective for three (3) years from its first registration.</ol>'+
							'</li>'+
							'<br>'+
							'<p><strong>Note period of coverage: </strong></p>'+
							'<li>Coverage starts on the 1st day of the following month <i>(Example: Date of Renewal: June 15, 2018; Effectivity Period: June 2018 –June 2019).</i> </li>'+
							'<li>If you wish to start your coverage today, you may do so by adding a premium for "period of coverage". </li>'+
							'<li>If you\'re applying for late registration, the effective date of your CTPL insurance will still be based on the previous effective date of your policy. Additional retro-active policy requirement: no loss or no accident from requested effectivity of policy up to the date of on-line purchase</li>'+
							'<br>'+
							'<p><strong>Call our HOTLINE number (02)876 4444…:</strong></p>'+
							'<li>If your application has been paid but your Plate Number is not yet authenticated or you haven’t received your Certificate of Cover, please call to confirm your payment and application.</li>'+
							'<br>'+
							'<p><strong>Additional charges</strong></p>'+
							'<li>For cancellation of authenticated CTPL policy, computation of additional charge is based on the number of days from the date of authentication to cancellation.</li>';

  	this.modal.open('Reminder',reminderMessageg,false,'form');

  }

}
