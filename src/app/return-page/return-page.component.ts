import { Component, OnInit ,EventEmitter, Input, Output, SimpleChanges, OnChanges   } from '@angular/core';
import {AuthService} from '../_services/auth.service';
import {IssuanceService} from '../_services/issuance.service';
import {NgxSpinnerService} from '../ngx-spinner';
import {AuthenticateDTO} from '../_objects/AuthenticateDTO';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import {ModalComponent} from './../modal/modal.component';
import {ModalService} from '../_services/modal.service';
import {TransactionService} from './../_services/transaction.service';
import {ResponseDTO} from '../_objects/ResponseDTO';
import {BpmService} from '../_services/bpm.service';

/*test*/
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {switchMap} from 'rxjs/operators';


@Component({
  selector: 'app-return-page',
  templateUrl: './return-page.component.html',
  styleUrls: ['./return-page.component.css']
})
export class ReturnPageComponent implements OnInit {

  numPoliza: any;
  authenticate : AuthenticateDTO;
  vpcTransactionNumber : any;
  transactionNumber : string;
  disablePrint : boolean = true;
  numPresupuesto : any;
  againLink : any;
  vpcMessage : any;


  constructor(private auth: AuthService,
              private route: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private scroll: ScrollToService,
              private modal : ModalService,
              private issuance : IssuanceService,
              private transService : TransactionService,
              private bpm : BpmService) { }

  ngOnInit() {

    this.numPoliza =  this.getParam('numPoliza');

      this.route.queryParams.subscribe(params => {
          if(params.vpc_Message != 'Approved'){
            window.location.href=params.AgainLink;
          }else{
            console.log("vpcTransactionNumber" + params.vpc_TransactionNo);
              this.vpcTransactionNumber =  params.vpc_TransactionNo;
              console.log(JSON.stringify(params));

              this.issuance.retrieveTransactionNumber(this.numPoliza).then(ret => {
                 console.log(ret);
                 this.transactionNumber = ret.numTransaction;
                 this.numPresupuesto = ret.numPresupuesto;   
                 if(ret.codProcess != 3){
                 this.authenticateCoc(this.numPoliza);
                 }
                 console.log("retrieved transaction number" + this.transactionNumber);
              });    

             
              this.issuance.validateGlobalPay(JSON.stringify(params)).then(obj => {
                console.log("return of validateGlobalPay");
                console.log(obj);
                 this.bpm.afterPayment("Yes", "", this.numPoliza,this.vpcTransactionNumber,obj.impRecibo).then(bpmRet => {
                   console.log(bpmRet);
                   console.log("Success payment");
                   this.spinner.hide();
                 }); 
              }).catch(error => {
                     this.modal.open('Error',error,false,null)
                     this.spinner.hide();

                   }
              ); 
          }
      }); 
   
    
  }

	/*created utility to get request parameters of url*/
  getParam(name){
    const results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if(!results){
      return 0;
    }
    return results[1] || 0;
  }

  authenticateCoc(numPoliza) {
    this.spinner.show();
    
    this.issuance.authenticateCoc(numPoliza).then(ret => {
      
      this.authenticate = ret;
      if(this.authenticate.errorMessage == null){
        this.disablePrint = false;
        const config: ScrollToConfigOptions = {
              target: 'print-comp'
        };

        setTimeout(() => {
          this.scroll.scrollTo(config);
        }, 200);
        
        /*insert transaction success here*/
        this.transService.insertTransaction("3","1",this.numPresupuesto,numPoliza,this.transactionNumber,null).then(ret => {
           var tranResponse = new ResponseDTO;
           tranResponse = ret;
          if (tranResponse.status == "1") {
            this.transactionNumber = tranResponse.message;
            this.auth.setTransactionNumber(this.transactionNumber);
            console.log(this.transactionNumber);
          } 
         });  
      } else {
         /*insert transaction fail here*/
        this.transService.insertTransaction("3","2",this.numPresupuesto,numPoliza,this.transactionNumber,null).then(ret => {
           var tranResponse = new ResponseDTO;
           tranResponse = ret;
          if (tranResponse.status == "1") {
            this.transactionNumber = tranResponse.message;
            this.auth.setTransactionNumber(this.transactionNumber);
              console.log("afterSET " + this.transactionNumber) ;  
            console.log(this.transactionNumber);
          } 
        });  
        this.modal.open("ERROR",this.authenticate.errorMessage,false,null);
      }

      this.spinner.hide();

    }).catch(error => {
               this.modal.open('Error',error,false,null)
               this.spinner.hide();

             }
    ); 
  }
}
