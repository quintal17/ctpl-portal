import {Injectable} from '@angular/core';
import { Client } from "../_objects/Client";
import { Vehicle } from "../_objects/Vehicle";;

@Injectable()
export class ValidatorService {

	

 validateClient(cli: Client) {
 	var head = "Please fill up the following required fields: <br/><br/>"; 
 	var body = "";
 	this.isNotNull(cli.ctplOption) ?  null : body += '<p>*Type of Certificate of Cover.'+ "</p>";
 	this.isNotNull(cli.fName) ?  null : body += '<p>*First Name'+ "</p>";
 	this.isNotNull(cli.lName) ?  null : body +=  '<p>*Last Name'+ "</p>";
 	this.isNotNull(cli.birthdate) ?  null : body +=  '<p>*Birth Date'+ "</p>";
 	this.isNotNull(cli.province) ?  null : body += '<p>*Province'+ "</p>";
 	this.isNotNull(cli.municipality) ?  null : body +='<p>*Municipality'+ "</p>";
 	this.isNotNull(cli.zipCode) ?  null : body +=  '<p>*Zip Code'+ "</p>";
 	this.isNotNull(cli.address1) ?  null : body +=  '<p>*Address'+ "</p>";
 	this.isNotNull(cli.codDoc) ?  null : body +=  '<p>*Document Code' + "</p>";
	this.isNotNull(cli.tipDoc) ?  null : body += '<p>*Document Type'+ "</p>";

	console.log(cli.birthdate + "birthdate");
 	return this.isNotNull(body) ? head+ body : "";
 }

 validateVehicle(veh: Vehicle) {
 	var head = "Please fill up the following required fields: <br/><br/>"; 
 	var body = "";

 	this.isNotNull(veh.mvNumber1st) ?  null : body += '<p>*MV Number I'+ "</p>";
 	this.isNotNull(veh.mvNumber2nd) ?  null : body += '<p>*MV Number II'+ "</p>";
 	this.isNotNull(veh.plateNumber) ?  null : body +='<p>*Plate Number'+ "</p>";
 	this.isNotNull(veh.engineNumber) ?  null : body +=  '<p>*Engine Number'+ "</p>";
 	this.isNotNull(veh.chassisNumber) ?  null : body +=  '<p>*Chassis Number'+ "</p>";
 	this.isNotNull(veh.make) ?  null : body += '<p>*Make'+ "</p>";
 	this.isNotNull(veh.model) ?  null : body +=  '<p>*Model'+ "</p>";
 	this.isNotNull(veh.variant) ?  null : body +=  '<p>*Vehicle Type'+ "</p>";
	this.isNotNull(veh.year) ?  null : body += '<p>*Year'+ "</p>";
	this.isNotNull(veh.subModel) ?  null : body += '<p>*Sub Model'+ "</p>";
	this.isNotNull(veh.typeOfUse) ?  null : body += '<p>*Type of Use'+ "</p>";
	this.isNotNull(veh.color) ?  null : body +=  '<p>*Color' + "</p>";

 	return this.isNotNull(body) ? head+ body : "";
  
 }


 validateContact(cli: Client) {
 	var head = "Please fill up the following required fields: <br/><br/>"; 
 	var body = "";

 	this.isNotNull(cli.mobileNumber) ?  null : body += '<p>*Mobile Number'+ "</p>";
 	this.isNotNull(cli.email) ?  null : body +=  '<p>*Email'+ "</p>";

 	return this.isNotNull(body) ? head+ body : "";
 }

 nxtLine(body: string) {
 	return this.isNotNull(body) ? '<br/>' : ''; ;
 }


 isNotNull(str : string) {

 	return !(str == null || str == undefined || str == "");
 }

}
