/*Client Model Used in form data binding.*/
export class Client { 
  fName: string;
  mName: string;
  lName: string;
  province: any;
  municipality: string;
  address1: string;
  address2: string;
  address3: string;
  codDoc: string;
  tipDoc: string;
  zipCode: string;
  suffix : string;
  ctplOption:string;
  mobileNumber: string;
  email: string;

  birthdate: string;

  provinceTxt : string;
  municipalityTxt : string;
  tipDocTxt: string;
  suffixTxt:string;

  constructor() {}


}