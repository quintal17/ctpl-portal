/*Vehicle Model class used in form data binding.*/
export class Vehicle {

  make: string;
  model: string;
  variant: string;
  mvNumber: string;
  plateNumber: string;
  engineNumber: string;
  chassisNumber: string;
  inceptionDate: string;
  expiryDate: string;
  color: string;
  year: string;
  subModel: string;
  typeOfUse: string;
  businessLine: string;
  numPoliza: string;
  zipCode: string;
  tipCocafRegistration:string;
  txtMotivoSpto:string;
  mvNumber1st: string;
  mvNumber2nd: string;


  makeTxt:string;
  modelTxt:string;
  variantTxt:string;
  subModelTxt:string;
  typeOfUseTxt:string;
  colorTxt:string;
}