export class AuthenticateDTO {
  
   assuredName  : string 
   assuredTin  : string 
   authNo  : string 
   chassisNo  : string 
   cocNo  : string 
   engineNo  : string 
   errorMessage  : string 
   expiryDate  : string 
   inceptionDate  : string 
   mvFileNo  : string 
   mvPremType  : string 
   mvType  : string 
   password  : string 
   plateNo  : string 
   premiumType  : string 
   regType  : string 
   successMessage  : string 
   taxType   : string 
   username  : string 

  constructor() {}


}