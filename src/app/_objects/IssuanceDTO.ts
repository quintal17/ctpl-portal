import {Client} from "./Client";
import {Vehicle} from "./Vehicle";
export class IssuanceDTO {


  fName: string;
  mName: string;
  lName: string;
  province: string;
  municipality: string;
  address1: string;
  address2: string;
  address3: string;
  codDoc: string;
  tipDoc: string;
  birthdate:string;
  suffix : string;

  mobileNumber: string;
  email: string;

  make: string;
  model: string;
  variant: string;
  mvNumber: string;
  plateNumber: string;
  engineNumber: string;
  chassisNumber: string;
  inceptionDate: string;
  expiryDate: string;
  color: string;
  year: string;
  subModel: string;
  typeOfUse: string;
  businessLine: string;
  numPoliza: string;
  zipCode: string;

  tipCocafRegistration:string;
  txtMotivoSpto:string;

  build(client: Client, vehicle: Vehicle) {

    this.fName = client.fName;
    this.mName = client.mName;
    this.lName = client.lName;
    this.province = client.province;
    this.municipality = client.municipality;
    this.address1 = client.address1;
    this.address2 = client.address2;
    this.address3 = client.address3;
    this.codDoc = client.codDoc;
    this.tipDoc = client.tipDoc;
    this.mobileNumber = client.mobileNumber;
    this.email = client.email;
    this.zipCode = client.zipCode;
    this.birthdate = client.birthdate;
    this.suffix = client.suffix;  

    this.make = vehicle.make;
    this.model = vehicle.model;
    this.variant = vehicle.variant;
    this.mvNumber = vehicle.mvNumber1st + vehicle.mvNumber2nd;
    this.plateNumber = vehicle.plateNumber;
    this.engineNumber = vehicle.engineNumber;
    this.chassisNumber = vehicle.chassisNumber;
    this.inceptionDate = vehicle.inceptionDate;
    this.expiryDate = vehicle.expiryDate;
    this.color = vehicle.color;
    this.year = vehicle.year;
    this.subModel = vehicle.subModel;
    this.typeOfUse = vehicle.typeOfUse;
    this.businessLine = vehicle.businessLine;
    this.numPoliza = vehicle.numPoliza;
    this.tipCocafRegistration = vehicle.tipCocafRegistration
    this.txtMotivoSpto = vehicle.txtMotivoSpto;
    
  }

  
}