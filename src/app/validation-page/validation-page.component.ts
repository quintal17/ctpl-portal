import {Component, OnInit , Output ,EventEmitter} from '@angular/core';
import {LovService} from './../_services/lov.service';
import {BpmService} from './../_services/bpm.service';
import {AuthService} from '../_services/auth.service';
import {IssuanceDTO} from '../_objects/IssuanceDTO';
import {ResponseDTO} from '../_objects/ResponseDTO';
import {paymentDTO} from '../_objects/PaymentDTO';
import {TransactionDTO} from '../_objects/TransactionDTO';
import {NgxSpinnerService} from '../ngx-spinner';
import {IssuanceService} from './../_services/issuance.service';
import {ModalService} from './../_services/modal.service';
import {TransactionService} from './../_services/transaction.service';


@Component({
  selector: 'app-validation-page',
  templateUrl: './validation-page.component.html',
  styleUrls: ['./validation-page.component.css']
})
export class ValidationPageComponent implements OnInit {

  validationCode: any;
  refNum: any;
  valid = "";
  policyNum: any;
  issuanceDto: IssuanceDTO;
  responseDTO: ResponseDTO;
  pDTO = new paymentDTO();
  errorMessage = "";
  provisonalErrorMessage:string=""

  tranDTO : TransactionDTO;
  transactionNumber : string;

  @Output() showPaymentPage = new EventEmitter<boolean>();
  
  constructor(private lov: LovService, 
              private auth: AuthService, 
              private bpm: BpmService, 
              private spinner: NgxSpinnerService, 
              private issuance: IssuanceService,
              private modal : ModalService,
              private transService: TransactionService,) {
  }

  ngOnInit() {
  }

  issuePolicy() {
    this.spinner.show();
    this.issuanceDto = this.auth.getissuanceDTO;
    console.log("Reference Number is: " + this.issuanceDto.numPoliza);
    console.log(this.issuanceDto);
    this.issuance.issuePolicy(this.issuanceDto).then(retDTO => {

      var quote = this.issuanceDto.numPoliza;
      
      this.transactionNumber = this.auth.getTransactionNumber;
      console.log(quote+ " quotes");
      console.log(retDTO);
      this.responseDTO = retDTO;
      console.log("Status is " + this.responseDTO.status);
      console.log("Message is " + this.responseDTO.message);
      if (this.responseDTO.status == "1") {
        console.log("Successful Policy Issuance~!");
        this.issuanceDto.numPoliza = this.responseDTO.message;
        console.log(this.issuanceDto.numPoliza);
        this.auth.setIssuanceDTO(this.issuanceDto);
        this.auth.setNumPoliza(this.issuanceDto.numPoliza);
        /*retrievePaymentPolicy*/
        this.issuance.retrievePaymentPolicy(this.issuanceDto.numPoliza).then(obj => {
          console.log(obj);
          this.pDTO = obj;
          this.auth.setPaymentDTO(this.pDTO);
         });  


          /*insert transaction here*/
        this.transService.insertTransaction("2","1",quote,this.issuanceDto.numPoliza,this.transactionNumber,"policy").then(ret => {
          var tranResponse = new ResponseDTO;
           tranResponse = ret;
          if (tranResponse.status == "1") {
            this.transactionNumber = tranResponse.message;
            console.log(this.transactionNumber);
            this.auth.setTransactionNumber(this.transactionNumber);
          } 
          });

        this.checkPolicyIfProvisional(this.issuanceDto.numPoliza);

        
      } else {
        /*Failed on policy issuance*/
        console.log("Failed Issuance~!");
          /*insert transaction here*/
        this.transService.insertTransaction("2","2",quote,null,this.transactionNumber,"policy").then(ret => {
           var tranResponse = new ResponseDTO;
           tranResponse = ret;
          if (tranResponse.status == "1") {
            this.transactionNumber = tranResponse.message;
            console.log(this.transactionNumber);
            this.auth.setTransactionNumber(this.transactionNumber);
          } 
          });
        /*send to policy central */ 
        this.bpm.processPolicyCentral(quote,"reference", this.responseDTO.message).then(bpmRet => {
          console.log(bpmRet);
          this.spinner.hide();
        });
        this.errorMessage = this.responseDTO.message;
        this.modal.open('Error',this.errorMessage,false,null);
      } 
      this.spinner.hide();
    });
  }

  validateCode() {
    var stat;
    this.refNum = this.auth.getRefNum;
    console.log(this.refNum + "refNum");
    console.log(this.auth.getTransactionNumber+"before validate");
   console.log(this.validationCode + "validationCode");
    this.bpm.validateCode(this.refNum, this.validationCode,this.auth.getTransactionNumber).then(ret => {
      console.log(ret);
      
      const retJson = JSON.parse(ret);
      

      if (retJson.codeValidationOutput == "1") {
        this.valid = "Code is Valid";
        console.log("Code is Valid");
        /*call policy issuance here*/
        this.issuePolicy();

      } else {
        console.log("Code is invalid. Please try again.");
        this.valid = "Code is invalid. Please try again.";
      }
    });
  }

  
  resendCode() {
    this.transactionNumber = this.auth.getTransactionNumber;
    this.refNum = this.auth.getRefNum;
    console.log("Resending new code for : " + this.refNum);
            this.bpm.generateCode(this.refNum,this.transactionNumber).then(bpmRet => {
            console.log(bpmRet);
            }); 
  }


  checkPolicyIfProvisional(numPoliza:string){

     this.issuance.getProvisionalError(numPoliza)
         .subscribe(data => {
           console.log(data)
           if(data == ""){
             this.showPaymentPage.emit(true);
           }else{
             this.modal.open('Error',data,false,null)
           }
         },
         err => {
            this.modal.open('Error',err,false,null)
         }
         );

  }

}
