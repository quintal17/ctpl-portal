import { Component, OnInit } from '@angular/core';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  title:string = "";
  message:string = "";
  retryButton:boolean = false;
  focusedElement:string = null;

  constructor(public activeModal: NgbActiveModal,private scroll: ScrollToService) {}

  ngOnInit() {
  }

  refresh(){
  	window.location.href=window.location.href;
  }

  closePop(){
    this.activeModal.close();
    const config: ScrollToConfigOptions = {
        target: this.focusedElement
    };
    setTimeout(() => {
        this.scroll.scrollTo(config);
    }, 150)
  }

}
