import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'requiredComponent',
  template:  `<div class="alert alert-danger"> {{inputNumber}} is required
                    </div>`
})
export class RequiredComponent  implements OnChanges { 
  myStringArray: string[];
  returnedString: string;
  @Input() inputNumber: number; 

  constructor(){
    this.myStringArray = ['First','Second','Third','Forth','Fifth','Sixth'];
    this.returnedString = 'number'+this.myStringArray[Number(this.inputNumber)];
  }

  ngOnChanges() {
    this.returnedString = 'number'+this.myStringArray[Number(this.inputNumber)];   
  }
}