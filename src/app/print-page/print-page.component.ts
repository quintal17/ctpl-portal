import { Component, OnInit ,EventEmitter, Input, Output, SimpleChanges, OnChanges   } from '@angular/core';

import {AuthService} from '../_services/auth.service';
import {IssuanceService} from './../_services/issuance.service';
import {NgxSpinnerService} from '../ngx-spinner';
import {BpmService} from '../_services/bpm.service';
import {ModalService} from '../_services/modal.service';

@Component({
  selector: 'app-print-page',
  templateUrl: './print-page.component.html',
  styleUrls: ['./print-page.component.css']
})
export class PrintPageComponent implements OnInit {

  @Input() numPoliza : string;
  @Input() disablePrint : boolean = false;
  

  constructor(private auth: AuthService,
              private spinner: NgxSpinnerService,
              private issuance: IssuanceService,
              private bpm: BpmService,
              private modal:ModalService) { }

  ngOnInit() {
  }
  
  printPolicyPdf(){
    this.spinner.show();
     this.issuance.printPolicy(this.numPoliza)
         .subscribe(data => {
           this.spinner.hide();
           var fileURL = URL.createObjectURL(data);
           window.open(fileURL);                     
         },
         err => {
            this.modal.open('ERROR',err,null,null);         
            console.log('Something went wrong!');
         }
         );

    	// this.bpm.afterPayment("Yes", "", this.numPoliza,"","").then(bpmRet => {
     //          console.log(bpmRet);
     //          console.log("Success payment");
        // });     

  }

  getClass(){
    if(this.disablePrint){
      return 'btn btn-print-disabled disabled btn-xl';
    }else{
      return 'btn btn-light btn-xl';
    }
  }

}
