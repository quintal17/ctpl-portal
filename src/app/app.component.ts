import {Component} from '@angular/core';
import {AuthService} from './_services/auth.service';
//import { saveAs } from 'file-saver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent { 
  title = 'app';
  constructor(private auth: AuthService ) {
    console.log("Application Start");
    this.auth.loginUser("any", "any").subscribe(data => {
        console.log(data.access_token);
        localStorage.setItem('ctplToken', data.access_token);
      });
//    var file = new File(["Hello, world!"], "hello world.txt", {type: "text/plain;charset=utf-8"});
//    saveAs(file);
  }
  
}
