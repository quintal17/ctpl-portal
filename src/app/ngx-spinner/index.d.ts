import { ModuleWithProviders } from '@angular/core';
export {NgxSpinnerComponent} from './ngx-spinner.component';
export {NgxSpinnerService} from './ngx-spinner.service';
export declare class NgxSpinnerModule {
    static forRoot(): ModuleWithProviders;
}
