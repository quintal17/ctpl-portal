import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient, HttpHeaders} from '@angular/common/http';
import {HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS,} from '@angular/common/http';
import {ModalModule} from 'ngx-bootstrap';
import {NgbModule,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpModule}            from '@angular/http';


/*Components*/
import {AppComponent} from './app.component';
import {IssuanceComponent} from './issuance/issuance.component';
import {AppRoutingModule} from './app.routing';
import {HeaderComponent} from './header/header.component';
import {ValidationPageComponent} from './validation-page/validation-page.component';
import {PaymentPageComponent} from './payment-page/payment-page.component';
import {PrintPageComponent} from './print-page/print-page.component';
import {RequiredComponent} from './_components/required.component';
import { CtplOptionComponent } from './issuance/ctpl-option/ctpl-option.component';
import { PersonalInformationComponent } from './issuance/personal-information/personal-information.component';
import { ContactInformationComponent } from './issuance/contact-information/contact-information.component';
import { VehicleInformationComponent } from './issuance/vehicle-information/vehicle-information.component';
import { InceptionDateComponent } from './issuance/inception-date/inception-date.component';
import { PricingComponent } from './issuance/pricing/pricing.component';
import { DatepickerPopupComponent } from './datepicker-popup/datepicker-popup.component';

import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {LoadingModule} from 'ngx-loading';
import {NgxSpinnerModule} from './ngx-spinner';

import { UppercaseDirective } from './uppercase.directive';
import { NumberOnly } from './_directives/number-only.directive';
import { LetterOnly } from './_directives/letter-only.directive';

/*Services*/
import {LovService} from './_services/lov.service';
import {AuthService} from './_services/auth.service';
import {BpmService} from './_services/bpm.service';
import {IssuanceService} from './_services/issuance.service';
import {ModalService} from './_services/modal.service';
import { BannerComponent } from './banner/banner.component';
import {TokenInterceptorService} from './_services/token-interceptor.service';
import {IdleTimeoutService} from './_services/idle-timeout.service';
import {AuthGuard} from './_guards/auth.guard';
import { ModalComponent } from './modal/modal.component';
import { ReturnPageComponent } from './return-page/return-page.component';
import { FooterComponent } from './footer/footer.component';
import {TransactionService} from './_services/transaction.service';
import { SummaryComponent } from './issuance/summary/summary.component';
import { FieldErrorDisplayComponent } from './field-error-display/field-error-display.component';
import { ValidatorService } from './_util/validator.component';


@NgModule({
  declarations: [
    AppComponent,
    IssuanceComponent,
    HeaderComponent,
    ValidationPageComponent,
    PaymentPageComponent,
    PrintPageComponent,
    RequiredComponent,
    BannerComponent,
    CtplOptionComponent,
    FieldErrorDisplayComponent,
    PersonalInformationComponent,
    ContactInformationComponent,
    VehicleInformationComponent,
    InceptionDateComponent,
    PricingComponent,
    DatepickerPopupComponent,
    ModalComponent,
    UppercaseDirective,
    ReturnPageComponent,
    FooterComponent,
    NumberOnly,
    LetterOnly,
    SummaryComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    //    ModalModule.forRoot(),
    NgbModule.forRoot(),
    ScrollToModule.forRoot(),
    LoadingModule,
    NgxSpinnerModule
    // Optionally you can set time for `idle`, `timeout` and `ping` in seconds.
    // Default values: `idle` is 600 (10 minutes), `timeout` is 300 (5 minutes) 
    // and `ping` is 120 (2 minutes).
    //UserIdleModule.forRoot({idle: 600, timeout: 300, ping: 120})  // (5 mins) (1 min) (1min)
  ],
  entryComponents:[
    ModalComponent
  ],
  providers: [
    LovService,
    AuthService,
    BpmService,
    IssuanceService,
    IdleTimeoutService,
    AuthGuard,
    NgbActiveModal,
    ModalService,
    TransactionService,
    ValidatorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
