import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
//import { LoginComponent } from './module/login/login.component';
//import { FooterComponent } from './shared/footer/footer.component';
//import { MenuComponent } from './shared/menu/menu.component';
//import { PolicyComponent } from './module/policy/policy.component';
//import { DashboardComponent } from './module/account/dashboard/dashboard.component';
//import {PolicyholderComponent } from './module/issuance/policyholder/policyholder.component';
//import {PolicydetailsComponent} from './module/Issuance/policydetails/policydetails.component';
//import {IssuanceComponent} from './module/Issuance/issuance/issuance.component';
//
//import { BpiLoginComponent } from './bpi/module/bpi-login/bpi-login.component';
//import { BdoLoginComponent } from './bdo/module/bdo-login/bdo-login.component';
//import { AuthGuard } from './_guards/auth.guard';
//import { SideNavComponent } from './shared/sidenav/sidenav.component';
//import { BdoDashboardComponent } from './bdo/module/bdo-dashboard/bdo-dashboard.component';

import {AppComponent} from './app.component';
import {IssuanceComponent} from './issuance/issuance.component';
import {HeaderComponent} from './header/header.component';
import {AuthGuard} from './_guards/auth.guard';
import {ReturnPageComponent} from './return-page/return-page.component';

const appRoutes: Routes = [
  {path: '', redirectTo: 'issuance', pathMatch: 'full', canActivate: [AuthGuard]},
//  {path: 'issuance', pathMatch: 'full', component: IssuanceComponent, canActivate: [AuthGuard]},
  {path: 'issuance', component: IssuanceComponent, canActivate: [AuthGuard]},
  {path: 'return-page', component: ReturnPageComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]

})
export class AppRoutingModule {


}
