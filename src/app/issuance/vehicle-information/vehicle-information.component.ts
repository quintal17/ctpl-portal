import { Component, OnInit ,EventEmitter, Input, Output, SimpleChanges, OnChanges   } from '@angular/core';
import { Vehicle} from '../../_objects/Vehicle';
import {LovService} from './../../_services/lov.service';
import {LOVRequestDTO} from './../../_objects/LOVRequestDTO';
import {NgxSpinnerService} from '../../ngx-spinner';
import {ValidatorService} from './../../_util/validator.component';
import {ModalService} from './../../_services/modal.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'vehicle-information',
  templateUrl: './vehicle-information.component.html',
  styleUrls: ['./vehicle-information.component.css']
})
export class VehicleInformationComponent implements OnInit {
  @Input() vehicle = new Vehicle();
  makeLOV: any[];	 
  colorLOV: any[];	
  modelLOV: any[];
  variantLOV: any[];
  yearLOV: any[];
  subModelLOV: any[];
  typeOfUseLOV: any[];

  proceedTicker : boolean = false;

  @Output() vehicleInfo = new EventEmitter<Vehicle>();


  constructor(private lov: LovService,
          private validator: ValidatorService,
          private spinner: NgxSpinnerService,
          private errmodal: ModalService,
          private scroll: ScrollToService,) { 
  	this.getMakeList();
  	this.getColorList();
  }

  ngOnInit() {
  }

  getVehicleInfo(vehicle:Vehicle){

    const errorMessage = this.validator.validateVehicle(vehicle);
    if(errorMessage == ""){

  	this.vehicleInfo.emit(vehicle)
    } else {
      this.spinner.hide();
      this.errmodal.open('Error',errorMessage,false,'pinf');

    }
  }

  getMakeList() {
    const dto = new LOVRequestDTO('A2100400', '5', 'COD_CIA~1|COD_RAMO~100');//make
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.makeLOV = lovs;
    });
  } 	
  
  getColorList() {
   this.spinner.show();
    const dto = new LOVRequestDTO('A2100800', '1', '');//codDocum
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.colorLOV = lovs;
      this.spinner.hide();
    });
  }

  makeOnchange(event:Event) {
    console.log('makeeeeeee ' + this.vehicle.make);
    this.getModelList();
    this.vehicle.model = undefined;
    this.vehicle.variant = undefined;
    this.vehicle.year = undefined;
    this.vehicle.subModel = undefined;
    this.vehicle.typeOfUse = undefined;
    this.vehicle.makeTxt = event.target['options'][event.target['options'].selectedIndex].text;
  }

  getModelList() {
    this.spinner.show();
    const dto = new LOVRequestDTO('A2100410', '5', 'COD_RAMO~100|COD_MARCA~' + this.vehicle.make + '|COD_CIA~1');//model
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.modelLOV = lovs;
      this.spinner.hide();
    });
  }	

  modelOnchange(event:Event) {
    console.log('modell' + this.vehicle.model);
    this.getVariantList();
    this.vehicle.variant = undefined;
    this.vehicle.year = undefined;
    this.vehicle.subModel = undefined;
    this.vehicle.typeOfUse = undefined;
    this.vehicle.modelTxt = event.target['options'][event.target['options'].selectedIndex].text;
  }

  getVariantList() {
    this.spinner.show();
    const dto = new LOVRequestDTO('A2100100',
      '4',
      'NUM_COTIZACION~1|COD_MARCA~' + this.vehicle.make +
      '|COD_MODELO~' + this.vehicle.model +
      '|COD_CIA~1'); //model
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.variantLOV = lovs;
      this.spinner.hide();
    });
  }

  variantOnchange(event:Event) {
    console.log(this.vehicle.make);
    this.getYearList();
    this.vehicle.year = undefined;
    this.vehicle.subModel = undefined;
    this.vehicle.typeOfUse = undefined;
    this.vehicle.variantTxt = event.target['options'][event.target['options'].selectedIndex].text;
  }		   

  getYearList() {
    this.spinner.show();
    const dto = new LOVRequestDTO('A2100430',
      '4',
      'NUM_COTIZACION~1|COD_MARCA~' + this.vehicle.make +
      '|COD_MODELO~' + this.vehicle.model + '|COD_TIP_VEHI~' + this.vehicle.variant + '|COD_CIA~1'); //year
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.yearLOV = lovs;
      this.spinner.hide();
    });

  }

  yearOnChange() {
    console.log(this.vehicle.year);
    this.getSubModelList();
    this.vehicle.subModel = undefined;
    this.vehicle.typeOfUse = undefined;

  }

  getSubModelList() {
    this.spinner.show();
    const dto = new LOVRequestDTO('A2100420',
      '4', 'NUM_COTIZACION~1|COD_MARCA~' + this.vehicle.make +
      '|COD_MODELO~' + this.vehicle.model + '|COD_TIP_VEHI~' + this.vehicle.variant + '|ANIO_SUB_MODELO~' + this.vehicle.year);
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.subModelLOV = lovs;
      this.spinner.hide();
    });
  }
  
  subModelOnChange(event:Event) {
    this.getTypeOfUseList();
    this.vehicle.typeOfUse = undefined;
    this.vehicle.subModelTxt = event.target['options'][event.target['options'].selectedIndex].text;
  }

  getTypeOfUseList() {
    this.spinner.show();
    const dto = new LOVRequestDTO('A2100200',
      '5', 'NUM_COTIZACION~1|COD_RAMO~100|COD_MARCA~' + this.vehicle.make +
      '|COD_MODELO~' + this.vehicle.model + '|COD_TIP_VEHI~' + this.vehicle.variant + '|ANIO_SUB_MODELO~' + this.vehicle.year);
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.typeOfUseLOV = lovs;
      this.spinner.hide();
    });
  }

  getTypeOfUse(event:Event){
   this.vehicle.typeOfUseTxt = event.target['options'][event.target['options'].selectedIndex].text; 
  }

  getColorTxt(event:Event){
   this.vehicle.colorTxt = event.target['options'][event.target['options'].selectedIndex].text;  
  }

   /*Check if  input parameter has initial value, therefore pre populate LOV*/
  ngOnChanges(changes: SimpleChanges) {
     try {
       if (changes.vehicle.currentValue.model != undefined) {
     
         this.prePopulate();
       }
      }
      catch(e) {
        console.log("exception lang");
      }  
  }

   prePopulate() {
    console.log("prepopulating vehicle-information");
    this.getModelList();
    this.getVariantList();
    this.getSubModelList();
    this.getYearList();
    this.getTypeOfUseList();
    this.proceedTicker = true;
    
  }

  tickPermission() {
      if(this.proceedTicker){
      this.proceedTicker = false;
    }else{
      this.proceedTicker = true;
    }
  }

  getValid(){
    return this.proceedTicker;
  
  }
    

}
