import { Component, OnInit , Input} from '@angular/core';
import {Client} from '../../_objects/Client' ;
import {Vehicle} from '../../_objects/Vehicle' ;
import {IssuanceDTO} from '../../_objects/IssuanceDTO' ;

@Component({
  selector: 'summary-details',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  @Input() client = new Client(); 	
  @Input() vehicle = new Vehicle(); 	
  @Input() issuanceDto = new IssuanceDTO(); 	

  constructor() { }

  ngOnInit() {
  }

}
