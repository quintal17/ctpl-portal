import { Component, OnInit ,EventEmitter, Input, Output, SimpleChanges, OnChanges, ViewChild   } from '@angular/core';
import {Client} from '../../_objects/Client';
import {LovService} from './../../_services/lov.service';
import {NgxSpinnerService} from '../../ngx-spinner';
import {LOVRequestDTO} from './../../_objects/LOVRequestDTO';
import {OptionListRequestDTO} from './../../_objects/OptionListRequestDTO';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {ValidatorService} from './../../_util/validator.component';
import {ModalService} from './../../_services/modal.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css']
})
export class PersonalInformationComponent implements OnInit, OnChanges {
  @ViewChild('personalForm') form;
  @Input() client = new Client();	
  @Input () ctplOption = '';

  provinceLOV: any[];
  municipalityLOV: any[];
  zipLOV: any[];
  codDocumLOV: any[];
  suffixLOV: any[];


  public preLoadFlag = false;
  
  @Output() clientInformation = new EventEmitter<Client>();

  constructor(private lov: LovService,
  			  private spinner: NgxSpinnerService,
          private route: ActivatedRoute,
          private formBuilder: FormBuilder,
          private validator: ValidatorService,
          private errmodal: ModalService,
          private scroll: ScrollToService,) { 

    console.log("personal-information init ");
  	this.getProvList();
    this.getCodDocumList();
    this.getSuffixLOV();
    this.spinner.show();
  }

  ngOnInit() {
    this.route.queryParams
      .filter(params => params.quotation)
      .subscribe(params => {
        /*Load quotation here and populate form.*/
        this.preLoadFlag = true;
        console.log("preLoadFlag is " + this.preLoadFlag);
        console.log(this.client.fName);
      });
  }

  
  onGetBirthdate(date:NgbDateStruct){
    this.client.birthdate = date.month +'/'+date.day+'/'+date.year;
  }
  
  getClientInformation(client:Client) {
  this.spinner.show();
  console.log(this.ctplOption + " ctplOption");
  client.ctplOption = this.ctplOption;
  const errorMessage = this.validator.validateClient(client);

  if (errorMessage == "") {
      this.spinner.hide();
      this.clientInformation.emit(client);  
  } else {
      this.spinner.hide();
      this.errmodal.open('Error',errorMessage,false,'pinf');
  }
    
  }

  getProvList() {
    this.spinner.show();
    const dto = new LOVRequestDTO('A1000100', '9', 'COD_PAIS~PHL');//codDocum
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.provinceLOV = lovs;
      this.spinner.hide();
    });
  }

  getCityList() {
    this.spinner.show();
    const dto = new LOVRequestDTO('A1000102', '7', 'cod_pais~PHL|cod_prov~' + this.client.province);//codDocum
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.municipalityLOV = lovs;
      this.spinner.hide();
    });
  }

  getZipList() {
    this.spinner.show();
    const dto = new LOVRequestDTO('A1000103', '1', 'cod_pais~PHL|cod_prov~' + this.client.province + '|cod_localidad~' + this.client.municipality);//zipCode
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.zipLOV = lovs;
      this.spinner.hide();
    });
  }

  getCodDocumList() {
    const dto = new LOVRequestDTO('A1002300', '3', 'COD_CIA~1');//codDocum
    const ic = this;
    this.lov.getLOV(dto).then(lovs => {
      ic.codDocumLOV = lovs;
    });

  }

  getSuffixLOV() {
    const dto = new OptionListRequestDTO('EN', 'TIPO_SUFIJO_NOMBRE', '999');
    const ic = this;
    this.lov.getOptionList(dto).then(lovs => {
      ic.suffixLOV = lovs;
    });

  }

  provOnChange(event: Event) {
    this.client.provinceTxt =  event.target['options'][event.target['options'].selectedIndex].text;
    this.getCityList();
  }

  municipalityChange() {
    this.client.municipalityTxt =  event.target['options'][event.target['options'].selectedIndex].text;
    this.getZipList();
  }

  prePopulate() {
    console.log("prepopulating personal-information");
    this.getCityList();
    this.getZipList();
  }

  getSuffix(event: Event){
    this.client.suffixTxt = event.target['options'][event.target['options'].selectedIndex].text;
  }

  getTipDocum(event: Event){
    this.client.tipDocTxt = event.target['options'][event.target['options'].selectedIndex].text;
  }
  /*Check if  input parameter has initial value, therefore pre populate LOV*/
  ngOnChanges(changes: SimpleChanges) {

     if (changes.client.currentValue.municipality != undefined) {
    
      this.prePopulate();
      // console.log(this.client.birthdate + "loaded birthday!");
      }
  }



  // isFieldValid(field: string) {
  // return !this.personalForm.get(field).valid && this.personalForm.get(field).touched;
  // }

  // displayFieldCss(field: string) {
  //   return {
  //   'has-error': this.isFieldValid(field),
  //   'has-feedback': this.isFieldValid(field)
  //   };
  // }





  
}
