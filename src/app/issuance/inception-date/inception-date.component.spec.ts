import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InceptionDateComponent } from './inception-date.component';

describe('InceptionDateComponent', () => {
  let component: InceptionDateComponent;
  let fixture: ComponentFixture<InceptionDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InceptionDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InceptionDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
