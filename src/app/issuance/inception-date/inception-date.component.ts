import { Component, OnInit ,EventEmitter, Input, Output, SimpleChanges, OnChanges } from '@angular/core';
import {NgbDateStruct,NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from './../../_services/auth.service';
import {InceptionComponentDTO} from './../../_objects/InceptionComponentDTO';


@Component({
  selector: 'inception-date',
  templateUrl: './inception-date.component.html',
  styleUrls: ['./inception-date.component.css']
})
export class InceptionDateComponent implements OnInit {
  
  @Input() inceptionDate = "";
  @Input() plateNumber = "";
  dateModel: NgbDateStruct;

  isCheck : boolean = false;

  inceptionComponentDto = new InceptionComponentDTO();

  monthValue : string;
  yearValue  : string;
  dayValue   : string;
  yearLOV = [];

  txtMotivoSpto : string;

  yearNow : number = new Date().getFullYear();
  
  showWarrantedNoLoss : boolean = false;
  showRequiredWarrantedNoLoss : boolean = false;
  
  @Output() inceptionDateOutput = new EventEmitter<InceptionComponentDTO>();

  constructor(private auth:AuthService,
              private dateParser:NgbDateParserFormatter  ) {
    
  }
  ngOnInit() {
    
    this.getYearLov();
    this.getMonthBasedOnPlate(this.plateNumber);
    this.showWarrantedNoLossCheckBox();
    console.log(this.inceptionDate+"  first inception date preloaded.");

  }

  getInceptionDate(){

    if((this.showWarrantedNoLoss && this.isCheck) || (!this.showWarrantedNoLoss)){

       this.dateModel = this.dateParser.parse(this.inceptionDate);
       this.inceptionComponentDto.dateModel = this.dateModel;
       this.inceptionComponentDto.txtMotivoSpto = this.txtMotivoSpto;
       this.showRequiredWarrantedNoLoss = false;

       this.inceptionDateOutput.emit(this.inceptionComponentDto);
    }else{
      this.showRequiredWarrantedNoLoss = true;
    }
   
  }

  getMonthBasedOnPlate(plate:string){
    var lastDigitPlate = plate.charAt(plate.length-1);
    switch (lastDigitPlate) {
      case "1":
        this.monthValue = "02"//FEBRUARY 1 ending plate 1
        break;
      case "2":
        this.monthValue = "03" //MARCH 1 ending plate 2
        break;
      case "3":
        this.monthValue = "04" //APRIL 1 ending plate 3
        break;
      case "4":
        this.monthValue = "05" //MAY 1 ending plate 4
        break;
      case "5":
        this.monthValue = "06" //JUNE 1 ending plate 5
        break;
      case "6":
        this.monthValue = "07" //JULY 1 ending plate 6
        break;
      case "7":
        this.monthValue = "08" //AUGUST 1 ending plate 7
        break;
      case "8":
        this.monthValue = "09" //SEPTEMBER 1 ending plate 8
        break;
      case "9":
        this.monthValue = "10" //OCTOBER 1 ending plate 9
        break;
      case "0":
        this.monthValue = "11" //NOVEMBER 1 ending plate 0
        break;                
      default:
        this.monthValue = "1"
        break;
    }

    this.inceptionDate = this.yearValue + '-' + this.monthValue + '-01';
    this.showWarrantedNoLossCheckBox();

  }

  getYearLov(){
    var yearLOV = []; 
    var endYear = this.yearNow - 1;
    for (var start = this.yearNow+1; start >= endYear; --start) {
      yearLOV.push(start);
    }
    this.yearValue = this.yearNow.toString();
    this.yearLOV = yearLOV;
    this.inceptionDate = this.yearNow + '-' + this.monthValue + '-01';

  }     

  changeYear(event: any){
    // yyyy/mm/dd
    this.yearValue = event.target.value;
    this.inceptionDate = event.target.value + '-' + this.monthValue + '-01';
    this.showWarrantedNoLossCheckBox();
  }  

  getExtDamage(){
    if(this.isCheck){
      this.isCheck = false;
      this.txtMotivoSpto = undefined;
    }else{
      this.isCheck = true;
      this.txtMotivoSpto = 'WARRANTED NO LOSS';
      this.showRequiredWarrantedNoLoss = false;  
    }
    
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {  
      if(propName == "plateNumber"){
        this.getMonthBasedOnPlate(this.plateNumber);
      }
    }
  }

  showWarrantedNoLossCheckBox(){
    this.txtMotivoSpto = undefined;
    var inceptionDt = new Date(this.inceptionDate);
    console.log(this.inceptionDate);
    var today = new Date();
    if(inceptionDt < today){
      this.showWarrantedNoLoss = true;
    }else{
      this.showWarrantedNoLoss = false;
    }
  }
}
