import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtplOptionComponent } from './ctpl-option.component';

describe('CtplOptionComponent', () => {
  let component: CtplOptionComponent;
  let fixture: ComponentFixture<CtplOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtplOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtplOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
