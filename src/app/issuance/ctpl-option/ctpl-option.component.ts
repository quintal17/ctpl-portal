import { Component, OnInit, Input ,Output ,EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'ctpl-option',
  templateUrl: './ctpl-option.component.html',
  styleUrls: ['./ctpl-option.component.css']
})
export class CtplOptionComponent implements OnInit {
  
  @Output() ctplOptionOutput = new EventEmitter<string>();
  @Input() checker : string;  
 
  checkNew = false;
  checkRenew = false;

  constructor() { }

  ngOnInit() {

  }

  getCtplOption(ctplOption:string){
  	console.log(ctplOption);
  	this.ctplOptionOutput.emit(ctplOption);
  }
  
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if(propName == "checker"){
        this.tickBox();
      }
    }
  }
  
  tickBox(){
   if(this.checker == 'R'){
    this.checkRenew = true;
    console.log(this.checkRenew + " renew");
   } else if(this.checker == 'N') {
    this.checkNew = true;
     console.log(this.checkNew + " check new ");
   }
  }

}
