import { Component, OnInit ,EventEmitter, Input, Output} from '@angular/core';
import {Client} from '../../_objects/Client';
import {NgxSpinnerService} from '../../ngx-spinner';
import {ValidatorService} from './../../_util/validator.component';
import {ModalService} from './../../_services/modal.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
@Component({
  selector: 'contact-information',
  templateUrl: './contact-information.component.html',
  styleUrls: ['./contact-information.component.css']
})
export class ContactInformationComponent implements OnInit {

  isEmailvalid : boolean = true;

  @Input() client = new Client();	

  @Output() clientContactInfo = new EventEmitter<Client>();


  constructor(private validator: ValidatorService,
          private spinner: NgxSpinnerService,
          private errmodal: ModalService,
          private scroll: ScrollToService,) { }

  ngOnInit() {
  }

  getClientContactInfo(client:Client){

    this.spinner.show();

    const errorMessage = this.validator.validateContact(client);
    if(errorMessage == ""){
    this.validateEmail(this.client.email)

      if(this.isEmailvalid){
        this.clientContactInfo.emit(client);
      }  
    } else {
       this.spinner.hide();
       this.errmodal.open('Error',errorMessage,false,'cinf');
    }
    
  }

  removeDotAndGreaterThan(){
    try{

        this.client.mobileNumber = this.client.mobileNumber.replace(".","");
        this.client.mobileNumber = this.client.mobileNumber.replace(">","");
      
    }catch{
            
    }
  }

  validateEmail(email:string){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(String(email).toLowerCase())){
      this.isEmailvalid = true;
    }else{
      this.isEmailvalid = false;
    }

    if(email == ""){
      this.isEmailvalid = true;
    }  

  }

}
