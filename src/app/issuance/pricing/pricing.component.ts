import { Component, OnInit,Input } from '@angular/core';
import {paymentDTO} from '../../_objects/PaymentDTO';

@Component({
  selector: 'pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {

  @Input() pDTO : paymentDTO;

  constructor() { }

  ngOnInit() {
  }

}
