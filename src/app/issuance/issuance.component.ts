import { Component, OnInit ,EventEmitter, Input, Output, SimpleChanges, OnChanges,ViewChild   } from '@angular/core';
import {LOVRequestDTO} from './../_objects/LOVRequestDTO';
import {LovService} from '../_services/lov.service';
import {IssuanceDTO} from '../_objects/IssuanceDTO';
import {AuthService} from '../_services/auth.service';
import {Observable} from 'rxjs/Observable';
import {LoadingModule} from 'ngx-loading';
import {NgxSpinnerService} from '../ngx-spinner';
import {paymentDTO} from '../_objects/PaymentDTO';
import {ResponseDTO} from '../_objects/ResponseDTO';
import {Client} from '../_objects/Client';
import {Vehicle} from "../_objects/Vehicle";
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {BpmService} from './../_services/bpm.service';
import {IssuanceService} from './../_services/issuance.service';
import {ModalService} from './../_services/modal.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import {ModalComponent} from './../modal/modal.component';
import {InceptionDateComponent} from './inception-date/inception-date.component';
import {TransactionDTO} from '../_objects/TransactionDTO';
import {TransactionService} from './../_services/transaction.service';
import {InceptionComponentDTO} from '../_objects/InceptionComponentDTO';

/*test*/
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {switchMap} from 'rxjs/operators';


const now = new Date();


@Component({
  selector: 'app-issuance',
  templateUrl: './issuance.component.html',
  styleUrls: ['./issuance.component.css']
})
export class IssuanceComponent implements OnInit {
  @ViewChild(InceptionDateComponent)
    private inception: InceptionDateComponent;

  /*LOVs declaration*/
  makeLOV: any[];
  modelLOV: any[];
  variantLOV: any[];
  yearLOV: any[];
  subModelLOV: any[];
  codDocumLOV: any[];
  typeOfUseLOV: any[];
  businessLineLOV: any[];
  provinceLOV: any[];
  municipalityLOV: any[];
  colorLOV: any[];
  zipLOV: any[];

  showPopUp(){
    //this.issuance.open();
    this.modal.open('Error','ErrorErrorErrorErrorErrorError',false,null);
  }

  showContactInfo: boolean = false;
  showVehicleInfo: boolean = false;
  showInceptionDate: boolean = false;
  showPricing: boolean = false;
  showValidationPage : boolean = false;
  showPaymentPage : boolean = false;  
  showPrintPage : boolean = false;  
  showSummary : boolean = false;

  /*assets*/
  logoPath = 'assets/images/mapfre-logo.png';
  libraryVar: any;

  public loading = false;
  dateModel: NgbDateStruct;
  date: {year: number, month: number};

  public preLoadFlag = false;



  /*Object instantiations*/
  issuanceDto = new IssuanceDTO();
  responseDTO = new ResponseDTO();
  client = new Client();
  vehicle = new Vehicle();
  pDTO = new paymentDTO();
  transaction = new TransactionDTO();
  inceptionDate = "";
  ctplOption = "";

  validationCode: any;
  refNum: any;
  hideValidationComponent = true;
  errorMessage: any;
  valid = "";
  policyNum: any;
  //  hidePaymentComponent = true;
  hidePaymentComponent: Observable<boolean>;
  hidePrintComponent: Observable<boolean>;
  transactionNumber : string;


  //   issuanceDto = new IssuanceDTO();


  constructor(private lov: LovService, 
              private auth: AuthService,  
              private bpm: BpmService,
              private spinner: NgxSpinnerService, 
              private issuance: IssuanceService,
              private scroll: ScrollToService,
              private modal : ModalService,
              private route: ActivatedRoute,
              private transService: TransactionService,
              private router: Router) {
    
  }
  onGetCtplOption(ctplOption:string){
    this.vehicle.tipCocafRegistration = ctplOption;
  }
  onGetInceptionDate(inceptionComponent: InceptionComponentDTO) {
    this.dateModel = inceptionComponent.dateModel;
    this.vehicle.txtMotivoSpto = inceptionComponent.txtMotivoSpto;
    this.constructDTO();
    this.showSummary = true;
    this.loading = true;
    this.spinner.show();
    this.issueQuotation();
  }

  onGetVehicleInfo(vehicle: Vehicle) {
    this.showInceptionDate = true;
    this.vehicle = vehicle;
  }

  onGetClientInformation(client: Client) {
    this.showContactInfo = true;
    this.client = client
    console.log(this.client);
  }

  onGetClientContactInfo(client: Client) {
    this.showVehicleInfo = true;
    this.client.mobileNumber = client.mobileNumber;
    this.client.email = client.email;
    console.log(this.client);
  }

  onShowPaymentPage(showPaymentPage:boolean){
    this.showPaymentPage = showPaymentPage;
    const config: ScrollToConfigOptions = {
          target: 'pcomp'
    };

    setTimeout(() => {
      this.scroll.scrollTo(config);
    }, 200);
  }


  onShowPrintPage(showPrintPage:boolean){
    this.showPrintPage = showPrintPage;
    const config: ScrollToConfigOptions = {
          target: 'print-comp'
    };

    setTimeout(() => {
      this.scroll.scrollTo(config);
    }, 200);
  }

  ngOnInit(): void {
    /*Anticipation if URL has query params. Pre-load data and scroll to task.* 
     * eg. http://localhost:8080/issuance?quotation=100102103135
     *
     */
    const quotationNo = this.getParam('quotation');
    const policyNo = this.getParam('numPoliza');
    /*check if with policy or quotation query param*/
    if(quotationNo != 0){
       this.preloadQuote(quotationNo);
      
    } else if(policyNo != 0) {
      this.preloadPolicy(policyNo);
    
    }

    
//    this.route.queryParams
//      .filter(params => params.quotation)
//      .subscribe(params => {
//        /*Load quotation here and populate form.*/
//        console.log("RequestParams" + params.quotation);
//        this.preloadQuote(params.quotation);
//      });

    this.hidePaymentComponent = this.auth.getHidePaymentComponent;
    this.hidePrintComponent = this.auth.getHidePrintComponent;
    setTimeout(() => {
      /** spinner ends after 8 seconds */
      this.checkWsdl();
      this.spinner.hide();
    }, 8000);
    
  }

  constructDTO() {
    /*Map Models to DTO */
    this.issuanceDto.build(this.client, this.vehicle);
    /*Date Parsing*/
    this.issuanceDto.inceptionDate = this.dateModel.month + '/' + this.dateModel.day + '/' + this.dateModel.year;
    this.issuanceDto.expiryDate = this.dateModel.month + '/' + this.dateModel.day + '/' + (this.dateModel.year + 1);
    console.log(this.issuanceDto);
  }

  issueQuotation() {
    this.issuance.issueQuote(this.issuanceDto).then(retDTO => {
      console.log(retDTO);
      this.responseDTO = retDTO;
      console.log("Status is " + this.responseDTO.status);
      console.log("Message is " + this.responseDTO.message);
      if (this.responseDTO.status == "1") {
        console.log("Successful Quotation~!");
        this.issuanceDto.numPoliza = this.responseDTO.message;
        this.refNum = this.responseDTO.message;
        console.log(this.issuanceDto.numPoliza);
        this.auth.setIssuanceDTO(this.issuanceDto);
        this.auth.setRefNum(this.refNum);
         /*insert transaction here*/
        this.transService.insertTransaction("1","1",this.issuanceDto.numPoliza,null,null,"quotation").then(ret => {
           var tranResponse = new ResponseDTO;
           tranResponse = ret;
          if (tranResponse.status == "1") {
            this.transactionNumber = tranResponse.message;
            this.auth.setTransactionNumber(this.transactionNumber);
            console.log(this.transactionNumber);
            this.bpm.generateCode(this.refNum,this.transactionNumber).then(bpmRet => {
            console.log(bpmRet);
            this.hideValidationComponent = false;
            }); 
          } 
          });

        /*retrieving payment and generating validation code*/
        this.issuance.retrievePaymentQuotation(this.refNum).then(obj => {
          this.pDTO = obj;
        });
        this.showPricing = true;
        this.showValidationPage = true;
        const config: ScrollToConfigOptions = {
          target: 'pricing'
        };
        
        setTimeout(() => {
          this.scroll.scrollTo(config);
        }, 200);
      } else {
        /*Failed on quotation issuance*/
        console.log("Failed Quotation~!");
          /*insert transaction here*/
        this.transService.insertTransaction("1","2",null,null,null,"quotation").then(ret => {
             var tranResponse = new ResponseDTO;
             tranResponse = ret;
            if (tranResponse.status == "1") {
            this.transactionNumber = tranResponse.message;
            this.auth.setTransactionNumber(this.transactionNumber);
            } 
         });
        this.errorMessage = this.responseDTO.message;

        this.modal.open('Error',this.errorMessage,false,null);
      } 


      this.spinner.hide();
    })
    .catch(error => {
                       console.log('atsfa ' + error);
                       this.modal.open('Error',error,false,null)
                       this.spinner.hide();

                     }
           );
  }

  /*function to pre-load quotation details when request URI has params to be call on ngOnInit()
   * @param quoteNum : quotation sent as request parameter in URI
   * 
   */
  preloadQuote(quoteNum: string) {
    this.preLoadFlag = true;
    console.log("pre-loading details for quotation number : " + quoteNum);
    this.spinner.show();
    this.showContactInfo = true;
    this.showVehicleInfo = true;
    this.showInceptionDate = true;
    this.showPricing = true;
    
    this.issuance.retrieveQuotationDetails(quoteNum).then(obj => {
      this.issuanceDto = obj;
      console.log(this.issuanceDto);
      this.client = this.buildClient(this.issuanceDto);
      this.vehicle = this.buildVehicle(this.issuanceDto);
      this.issuance.retrieveQuoteTransactionNumber(quoteNum).then(ret => {
       console.log(ret);
       this.transactionNumber = ret.numTransaction;
       /* call all setter*/
       this.auth.setTransactionNumber(this.transactionNumber);
       this.auth.setRefNum(ret.numPresupuesto);
       this.issuanceDto.numPoliza = ret.numPresupuesto;
       this.auth.setIssuanceDTO(this.issuanceDto);
       this.flowControl(ret); 
       });
    });
  }
  
   /*function to pre-load policy details when request URI has params to be call on ngOnInit()
   * @param polNum : policy sent as request parameter in URI
   * 
   */
  preloadPolicy(polNum: string) {
    this.preLoadFlag = true;
    console.log("pre-loading details for policy number : " + polNum);
    this.spinner.show();
    this.showContactInfo = true;
    this.showVehicleInfo = true;
    this.showInceptionDate = true;
    this.showPricing = true;
    this.issuance.retrievePolicyDetails(polNum).then(obj => {
      this.issuanceDto = obj;
      console.log(this.issuanceDto);
      this.client = this.buildClient(this.issuanceDto);
      this.vehicle = this.buildVehicle(this.issuanceDto);
      this.issuance.retrieveTransactionNumber(polNum).then(ret => {
       console.log(ret);
       this.transactionNumber = ret.numTransaction;
       this.showValidationPage = true;
       this.issuance.retrievePaymentPolicy(polNum).then(obj => {
        this.pDTO = obj;
        this.spinner.hide();
       });   
        const config: ScrollToConfigOptions = {
          target: 'pcomp'
          };
      });
    });
  }
  

  checkWsdl(){

    this.issuance.checkWsdl()
         .subscribe(data => {
           console.log(data)
           var error = data;
              if(error != ""){
                this.modal.open('Error',error,true,null)
              }
         },
         err => {
            this.modal.open('Error',err,false,null)
         }
         );
  }

  
  buildClient(dto: IssuanceDTO) {
    var client = new Client();
    client.fName = dto.fName;
    client.mName = dto.mName;
    client.lName = dto.lName;
    client.address1 = dto.address1;
    client.address2 = dto.address2;
    client.address3 = dto.address3;
    client.codDoc = dto.codDoc;
    client.tipDoc = dto.tipDoc;
    client.municipality = dto.municipality;
    client.province = dto.province;
    client.email = dto.email;
    client.mobileNumber = dto.mobileNumber;
    client.zipCode = dto.zipCode;
    /*set formatted date*/
    client.birthdate = dto.birthdate;

    return client;
  }

  buildVehicle(dto: IssuanceDTO) {
    var vehicle = new Vehicle();
    vehicle.make = dto.make;
    vehicle.model = dto.model;
    vehicle.color = dto.color;
    vehicle.year = dto.year;
    vehicle.subModel = dto.subModel;
    vehicle.typeOfUse = dto.typeOfUse;
    vehicle.variant = dto.variant;
    vehicle.chassisNumber = dto.chassisNumber;
    vehicle.engineNumber = dto.engineNumber;
    vehicle.mvNumber = dto.mvNumber;
    vehicle.plateNumber = dto.plateNumber;
    vehicle.inceptionDate = dto.inceptionDate;
    vehicle.tipCocafRegistration = dto.tipCocafRegistration;
    vehicle.txtMotivoSpto = dto.txtMotivoSpto;
    vehicle.inceptionDate = dto.inceptionDate;
    
    this.ctplOption = dto.tipCocafRegistration;

    return vehicle;
  }
  
  
  /*created utility to get request parameters of url*/
  getParam(name){
    const results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if(!results){
      return 0;
    }
    return results[1] || 0;
  }
  /*save point flow control*/
  flowControl(ret : any){
    
   if(ret.codProcess == 1  && ret.tipStatus == 1 ) {
     /*Quotation is for code validation scroll to validation component */
     this.showValidationPage = true;        
//          this.modal.open('Alert',"QUOTATION IS FOR VALIDATION");  
     this.issuance.retrievePaymentQuotation(ret.numPresupuesto).then(obj => {
        this.pDTO = obj;
        this.spinner.hide();
        const config: ScrollToConfigOptions = {
            target: 'vcomp'
          };
        setTimeout(() => {
            this.scroll.scrollTo(config);
          }, 150);  
     });   
     
   } else if(ret.codProcess == 2) {
      /*Policy is For Payment, redirect to return page*/
     console.log("POLICY IS FOR PAYMENT: " + ret.codProcess + " :: "+ ret.numPoliza);
     this.showPaymentPage = true;
     this.issuance.retrievePaymentPolicy(ret.numPoliza).then(obj => {
        console.log(obj);
        this.pDTO = obj;
        this.auth.setPaymentDTO(this.pDTO);
        this.auth.setNumPoliza(ret.numPoliza);
        console.log("b4 setting pdto");  
        console.log(this.auth.getPaymentDTO);  
        console.log(this.auth.getNumPoliza);
        this.spinner.hide();
        const config: ScrollToConfigOptions = {
            target: 'pcomp'
        };
        setTimeout(() => {
            this.scroll.scrollTo(config);
          }, 150);  
     }); 
       
   } else if(ret.codProcess == 3  && ret.tipStatus == 1 ) {
      /*Policy is paid already, redirect to return page*/
     this.router.navigate(['/return-page'], { queryParams: {numPoliza: ret.numPoliza}});
     this.spinner.hide();
     
   } else{
     this.modal.open('Error',"ERROR",false,null);  
   }
  }

}