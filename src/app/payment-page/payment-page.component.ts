import {Component, OnInit} from '@angular/core';

import {LovService} from './../_services/lov.service';
import {AuthService} from '../_services/auth.service';
import {BpmService} from '../_services/bpm.service';
import {IssuanceDTO} from '../_objects/IssuanceDTO';
import {paymentDTO} from '../_objects/PaymentDTO';
import {ResponseDTO} from '../_objects/ResponseDTO';
import {IssuanceService} from './../_services/issuance.service';
import {NgxSpinnerService} from '../ngx-spinner';


import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-payment-page',
  templateUrl: './payment-page.component.html',
  styleUrls: ['./payment-page.component.css']
})
export class PaymentPageComponent implements OnInit {

  refNum: any;
  pDTO: paymentDTO;
  responseDTO = new ResponseDTO();
  numPoliza: any;

  constructor(private lov: LovService, 
              private auth: AuthService, 
              private bpm: BpmService, 
              private spinner: NgxSpinnerService,
              private issuance: IssuanceService,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit() {

  }

  validatePayment() {
    console.log(this.auth.getNumPoliza+ " numsss");
    this.numPoliza = this.auth.getNumPoliza;
    this.spinner.show();
    this.pDTO = this.auth.getPaymentDTO;
    console.log(this.pDTO);
    this.auth.sethidePrintComponent(true);
    this.pDTO.numPoliza = this.auth.getNumPoliza;
   // localStorage.setItem('numPoliza', this.pDTO.numPoliza);
    /*Call payment service here before proceeding validation*/
    this.issuance.processPayment(this.pDTO).then(ret => {
          
      this.responseDTO = ret;
      console.log(this.responseDTO.message);
      window.open(this.responseDTO.message, "_self");
      console.log("payment is processed");
    }); 
  }

  skip(){
    console.log(this.auth.getNumPoliza+ " numsss");
    this.numPoliza = this.auth.getNumPoliza;
    this.spinner.show();
    this.router.navigate(['/return-page'], { queryParams: {numPoliza: this.numPoliza}});
  }

}
