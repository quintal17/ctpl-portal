import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {NgxSpinnerService} from '../ngx-spinner';
import {IssuanceDTO} from '../_objects/IssuanceDTO';
import {Http, ResponseContentType} from '@angular/http';
import {TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, API_URL, BPM_URL,AUTH_TOKEN,BPM_USER,BPM_PASSWORD} from '../_constant/app.constant';
import {ModalService} from './modal.service';
import {paymentDTO} from '../_objects/PaymentDTO';


export const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class AuthService {
  /**
   *BPM Service Constants  
  */
  private bpmUser = 'Administrator';
  private bpmPassword = 'bpmapp2017';
  private bpmWSURL = BPM_URL + '/rest/mphCTPLIssuance/general/ws/CTPLOnlineWs';

  private printingURL = 'mivo/PrintReport/quoteprint?';

  // public url = 'http://localhost:8080/api';
  // public AUTH_TOKEN = 'http://localhost:8080/oauth/token';
  // ==========PROD ENDPOINTS======================// uncomment when generating war file
  
  //public url = '/ctplonline/api'
  //public AUTH_TOKEN = '/ctplonline/oauth/token';
  // ==========END PROD===========================//
  
  // ============DEV ENDPOINTS=====================//comment when generating war file

    public url = 'api';
    public AUTH_TOKEN = '/oauth/token';
  // ============END DEV==========================//

  //  helper = new JwtHelperService();
  public tokenExpired = new BehaviorSubject<boolean>(true);
  private currentToken = new BehaviorSubject<any>({});
  private loggedIn = new BehaviorSubject<boolean>(false);
  private issuanceDTO = new BehaviorSubject<IssuanceDTO>(new IssuanceDTO());
  private refNum = new BehaviorSubject<any>({});

  private numPoliza = new BehaviorSubject<any>({});
  private hidePaymentComponent = new BehaviorSubject<any>(false);
  private hidePrintComponent = new BehaviorSubject<any>(false);
  private disableValidateBtn = new BehaviorSubject<any>(false);
  private inceptionDate = new BehaviorSubject<any>({});
  private pDTO = new BehaviorSubject<paymentDTO>(new paymentDTO());
  private transactionNumber = new BehaviorSubject<string>("");



  constructor(private router: Router, private http: HttpClient,private modal:ModalService,private spinner: NgxSpinnerService ) {
  }

  doBPMCalls(param: any) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    headers = headers.set('authorization', 'Basic ' + btoa(BPM_USER + ':' + BPM_PASSWORD));
    headers = headers.set(InterceptorSkipHeader, '');
    return this.http.post(BPM_URL, param, {headers})
      .toPromise()  
      .then(response => response)
      .catch(this.handleError);
  }

  downloadPDF(refNum: any): any {
    const body = `numPresupuesto=${encodeURIComponent(refNum)}`;
    //    2001800000003
    return this.http.get("http://localhost:8080/api/PrintReport/policyprint?numPoliza=" + refNum, {responseType: 'blob'})
      .map(res => {
        console.log(res + "retByte");
        return new Blob([res], {type: 'application/pdf', });
      });
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  get CurrentToken() {
    return this.currentToken.asObservable();
  }

  setLogin(login: boolean) {
    this.loggedIn.next(login);
  }

  setRefNum(refNum: any) {
    this.refNum.next(refNum);
  }

  setNumPoliza(numPoliza: any) {
    this.numPoliza.next(numPoliza);
  }

  setCurrentToken(token) {
    this.currentToken.next(token);
  }

  clearToken() {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      localStorage.removeItem('user');
    }
  }

  loginUser(username: string, password: string) {
    console.log("Logging in User : " + username);
    const as = this;
    const body = `username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}&grant_type=password`;

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    headers = headers.set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD));
    headers = headers.set(InterceptorSkipHeader, '');

    return this.http.post(AUTH_TOKEN, body, {headers})
      .map((res: any) => {
        if (res) {
          return res;
        }
      }).catch((err: any) => {
        console.log('An error occurred:', err.error);
        return err.error;
      });
  }

  doWhatever(param: any, endpoint: string): Promise<any> {

    return this.http.post(API_URL + endpoint, param)
      .toPromise()
      .then(response => response)
      .catch(this.handleError);

  }

  private handleError(error: any): Promise<any> {
    // this.spinner.hide();
    // this.modal.open('Error',error.message);
    console.error('An error    occurred', error);
    return Promise.reject(error.message || error);
  }

  login() {
    this.loggedIn.next(true);
    window.location.reload(true);
    this.router.navigate(['']);
  }

  logout() {
    this.clearToken();
    this.loggedIn.next(false);
    window.location.reload(true);
    this.router.navigate(['login']);
  }


  setIssuanceDTO(issuanceDto: IssuanceDTO) {
    this.issuanceDTO.next(issuanceDto);
  }

  sethidePaymentComponent(stat: boolean) {
    this.hidePaymentComponent.next(stat);
    //    const element = document.querySelector('#pcomp');
    //    element.scrollIntoView();
  }

  sethidePrintComponent(stat: boolean) {
    this.hidePrintComponent.next(stat);
  }

  setInceptionDate(inceptionDate: any) {
    this.inceptionDate.next(inceptionDate);
  }

  setPaymentDTO(pdto : paymentDTO) {
    this.pDTO.next(pdto);
  }

  setTransactionNumber(transactionNumber: any) {
    this.transactionNumber.next(transactionNumber);
  }

  setDisableValidateBtn(disabled) {
    this.disableValidateBtn.next(disabled);
  }

  get getHidePaymentComponent() {
    return this.hidePaymentComponent.asObservable();
  }

  get getHidePrintComponent() {
    return this.hidePrintComponent.asObservable();
  }


  get getissuanceDTO() {
    return this.issuanceDTO.getValue();
  } 

  get getRefNum() {
    return this.refNum.getValue();
  }

  get getNumPoliza() {
    return this.numPoliza.getValue();
  }

  get getInceptionDate(){
    return this.inceptionDate.getValue();
  }

  get getPaymentDTO() {
    return this.pDTO.getValue();
  }

  get getTransactionNumber() {
    return this.transactionNumber.getValue();
  } 
  
  get getDisableValidateBtn() {
    return this.disableValidateBtn.getValue();
  } 

}
