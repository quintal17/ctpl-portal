import { Injectable } from '@angular/core';
import {IssuanceService} from '../_services/issuance.service';
import {TransactionDTO} from '../_objects/TransactionDTO';

@Injectable() 
export class TransactionService {


  constructor(private issuance: IssuanceService) { }


  
  
  /*insert transaction service* 
   * 
   * @param process :  process number(1,2,3,4).
   * @param status :  1 : 2 (fail : success).
   * @param quotationNumber : ref num;
   * @param policyNumber : policy number;
   * @param transaction : transaction number;
   * @param issuanceType : type of issuance;
   */
  insertTransaction(process : string, status: string, quotationNumber: string, policyNumber: string, transaction: string,issuanceType : string ) {
    var dto = new TransactionDTO;
    dto.process = process;
    dto.status = status;
    dto.quotationNumber = quotationNumber;
    dto.policyNumber = policyNumber;
    dto.transactionNumber = transaction;
    
    const today = this.getCurrentDate();
    
    if(issuanceType == "quotation"){
    dto.fecPresupuesto = today;
    } else if (issuanceType == "policy") {
    dto.fecPoliza = today;
    } else {
    
    }
    
    return  this.issuance.insertTransaction(dto);
  }
  
  
  getCurrentDate(){
    var today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth()+1; //January is 0!
    const yyyy = today.getFullYear();
    console.log( " now is " + mm+'/'+dd+'/'+yyyy);
    return mm+'/'+dd+'/'+yyyy;
  }
}
