import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response,ResponseContentType} from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../_services/auth.service';
import {LOVRequestDTO} from '../_objects/LOVRequestDTO';
import {IssuanceDTO} from '../_objects/IssuanceDTO';
import {paymentDTO} from '../_objects/PaymentDTO';
import {ResponseDTO} from '../_objects/ResponseDTO';
import {AuthenticateDTO} from '../_objects/AuthenticateDTO';
import {TransactionDTO} from '../_objects/TransactionDTO';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, API_URL, BPM_URL,AUTH_TOKEN,BPM_USER,BPM_PASSWORD} from '../_constant/app.constant';


@Injectable()
export class IssuanceService {

  private client = new BehaviorSubject<any>({}); 
  private inceptionDate = new BehaviorSubject<any>({}); 
  private personalInformation = new BehaviorSubject<any>({}); 
  private vehicleInformation = new BehaviorSubject<any>({});
  
  constructor(private auth: AuthService,
              private http: HttpClient) {}

  issueQuote(dto: IssuanceDTO): Promise<ResponseDTO> {
    console.log("issuing quotation for " + dto.fName);
    return this.auth.doWhatever(dto, '/issueQuote').then(obj => obj as ResponseDTO);
  }

  issuePolicy(dto: IssuanceDTO): Promise<ResponseDTO> {
     console.log("issuing policy for " + dto.fName);
    return this.auth.doWhatever(dto, '/issuePolicy').then(obj => obj as ResponseDTO);
  }

  retrievePaymentQuotation(numPoliza: string): Promise<paymentDTO> {
    console.log("retrieving payment of + " + numPoliza);
    return this.auth.doWhatever(numPoliza, '/retrievePaymentQuotation').then(obj => obj as paymentDTO);
  }

  retrievePaymentPolicy(numPoliza: string): Promise<paymentDTO> {
    console.log("retrieving payment of + " + numPoliza);
    return this.auth.doWhatever(numPoliza, '/retrievePaymentPolicy').then(obj => obj as paymentDTO);
  }

  printPolicy(numPoliza: string) {
     // console.log("Adding token as request header.");
     // const token = localStorage.getItem('ctplToken');
     //  let ctpltoken = '';
     //  if (token) {
     //    ctpltoken = `bearer ${token}`;
     //  }
     // let myHeaders = new Headers();
     // myHeaders.append('Authorization', ctpltoken);    

     // return this.http.post('http://localhost:8080/api/PrintReport/policyprint', numPoliza,
     //        { responseType: ResponseContentType.Blob,headers : myHeaders
     //          }).map((res : Response)  => {
     //        return new Blob([res.blob()], { type: 'application/pdf' })
     // });
     return this.http.post(API_URL+'/PrintReport/policyprint',numPoliza,{responseType:"blob"})
                .map((res : Blob)  => {
                  return new Blob([res], { type: 'application/pdf' });
                });
  }
  
  retrieveQuotationDetails(numPoliza: string): Promise<IssuanceDTO> {
    console.log("retrieving details of + " + numPoliza);
    return this.auth.doWhatever(numPoliza, '/retrieveQuotationDetails').then(obj => obj as IssuanceDTO);
  }
  
  retrievePolicyDetails(numPoliza: string): Promise<IssuanceDTO> {
    console.log("retrieving details of + " + numPoliza);
    return this.auth.doWhatever(numPoliza, '/retrievePolicyDetails').then(obj => obj as IssuanceDTO);
  }

  processPayment(pDTO: paymentDTO): Promise<ResponseDTO> {
    console.log("processing payment...");
    console.log(pDTO);
    return this.auth.doWhatever(pDTO, '/Payment/Request').then(obj => obj as ResponseDTO);
  }

  authenticateCoc(numPoliza: string): Promise<AuthenticateDTO> {
    console.log("processing authentication...");
    return this.auth.doWhatever(numPoliza, '/cocaf/register').then(obj => obj as AuthenticateDTO);
  }

  checkWsdl() {  
    return this.http.post(API_URL+'/cocaf/checkWsdl','',{responseType:"text"})
               .map((res : string)  => {
                  return res;
                });
  }

  insertTransaction(tranDTO: TransactionDTO): Promise<ResponseDTO> {
    console.log("inserting transaction for : " );
    console.log(tranDTO);
    return this.auth.doWhatever(tranDTO, '/insertTransaction').then(obj => obj as ResponseDTO);
  }

  testBPM(numPoliza: string): Promise<string> {
    console.log("TestingBPM + " + numPoliza);
    return this.auth.doWhatever(numPoliza, '/testBPM').then(obj => obj as string);
  }

   processBPM(param: string): Promise<string> {
    console.log("generateCodeBPM + " + param);
    return this.auth.doWhatever(param, '/processBPM').then(obj => obj as string);
  }
  
  retrieveTransactionNumber(param: string): Promise<any> {
    console.log("retrieving transaction number for + " + param);
    return this.auth.doWhatever(param, '/retrieveTransaction').then(obj => obj as any);
  }
  
  retrieveQuoteTransactionNumber(param: string): Promise<any> {
    console.log("retrieving transaction number for + " + param);
    return this.auth.doWhatever(param, '/retrieveQuoteTransaction').then(obj => obj as any);
  }

  validateGlobalPay(param: string): Promise<any> {
    console.log("validateGlobalPay");
    return this.auth.doWhatever(param, '/validateGlobalPay').then(obj => obj as any);
  }

  getProvisionalError(numPoliza: string){
    console.log("getProvisionalError");
    return this.http.post(API_URL+'/getProvisionalError',numPoliza,{responseType:"text"})
               .map((res : string)  => {
                  return res;
                });
  }

}
