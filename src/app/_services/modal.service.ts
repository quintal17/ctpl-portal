import { Injectable } from '@angular/core';
import {ModalComponent} from '../modal/modal.component';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class ModalService {
  closeResult: string;

  constructor(private modalService: NgbModal) { }

  open(title:string,message:string,hasRetryButton:boolean,focusedElement:string) {
    const modalRef = this.modalService.open(ModalComponent,{ backdrop: 'static' ,keyboard: false})
    modalRef.componentInstance.title=title;
    modalRef.componentInstance.message=message;
    modalRef.componentInstance.retryButton=hasRetryButton; 
    modalRef.componentInstance.focusedElement=focusedElement; 
  }

  openFormatted(title:string,message:string) {
    const modalRef = this.modalService.open(ModalComponent,{ backdrop: 'static' ,keyboard: false})
    modalRef.componentInstance.title=title;
    modalRef.componentInstance.message=message; 
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
