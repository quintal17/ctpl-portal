import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {API_URL} from '../../constant';
import {Observable} from 'rxjs';
import {AuthService} from '../_services/auth.service';
import {LOVRequestDTO} from '../_objects/LOVRequestDTO';
import {OptionListRequestDTO} from '../_objects/OptionListRequestDTO';
import {IssuanceDTO} from '../_objects/IssuanceDTO';
import {paymentDTO} from '../_objects/PaymentDTO';
import {ResponseDTO} from '../_objects/ResponseDTO';


@Injectable()
export class LovService {
  lovEndpoint = 'testEndpoint';
  constructor(private auth: AuthService) {}

  getLOV(dto: LOVRequestDTO): Promise<any[]> {
    return this.auth.doWhatever(dto, '/getLOV').then(objArr => objArr as any[]);
  }

  getOptionList(dto: OptionListRequestDTO): Promise<any[]> {
    return this.auth.doWhatever(dto, '/getOptionList').then(objArr => objArr as any[]);
  }

}
