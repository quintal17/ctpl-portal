import { Injectable } from '@angular/core';
import {AuthService} from '../_services/auth.service';
import {IssuanceService} from '../_services/issuance.service';

@Injectable() 
export class BpmService {

  constructor(private auth: AuthService, private issuance: IssuanceService) { }


   /*Generation of Code call to BPM WS* 
   * 1st call
   * @Param refNum : quotation number generated on issue quotation.
   * @return : BPM Response body.
   */
 /* generateCode(refNum: string, transactionNum : string) {
    console.log("Generating code for Reference Number: " + refNum);
    const body = `referenceNumber=${encodeURIComponent(refNum)}&task=${encodeURIComponent('GenerateValidationCode')}&numTransactionId=${encodeURIComponent(transactionNum)}`
    return this.auth.doBPMCalls(body).then(returned => returned as any);
  }*/

  /*BPM WS to validate code*
   * 2nd call
   * @Param refNum : quotation number generated on issue quotation.
   * @Param validationCode : code input by user to validate his/her email or cell number.
   */
//  validateCode(refNum: string, validationCode: string, transactionNum : string) {
//    console.log("validating code : " + validationCode);
//    const body = `validationCode=${encodeURIComponent(validationCode)}&referenceNumber=${encodeURIComponent(refNum)}&task=${encodeURIComponent('ValidateCode')}&numTransactionId=${encodeURIComponent(transactionNum)}`
//    return this.auth.doBPMCalls(body).then(returned => returned as any);
//  }

  /*BPM WS to be called after policy issuance* 
   * 3rd call
   *  @param stat : status of issuance batch process "Yes" if success, "No" if unsuccessful.
   *  @param refNum :  quotation number generated on issue quotation.
   */
//  afterIssuance(stat: string, refNum: string) {
//    console.log("validatedCodeAndBP parameters " + stat + " " + refNum);
//
//    const body = `success=${encodeURIComponent(stat)}&referenceNumber=${encodeURIComponent(refNum)}&task=${encodeURIComponent('validatedCodeAndBP')}`
//    return this.auth.doBPMCalls(body).then(returned => returned as any);
//  }


  /*BPM WS to be called after payment* 
   4th call
   * @param stat : status of payment  "Yes" if success, "No" if unsuccessful.
   * @param refNum :  quotation number generated on issue quotation.
   * @param policyNum :  Policy number generated after issuance batch process.
   */
//  afterPayment(stat: string, refNum: string, policyNum: string) {
//    console.log("ValidatePayment parameters " + stat + "  " + refNum + " " + policyNum);
//    const body = `paymentStatus=${encodeURIComponent(stat)}&referenceNumber=${encodeURIComponent(refNum)}&numPoliza=${encodeURIComponent(policyNum)}&task=${encodeURIComponent('ValidatePayment')}`
//    return this.auth.doBPMCalls(body).then(returned => returned as any);
//  }
  
  
  /*BPM WS to be called when user wants to receive another validation code.* 
   optional call
   * 
   * @param refNum :  quotation number generated on issue quotation.
   * 
   */
  resendCode(refNum: string) {
    console.log("resendCode parameters "+ refNum);
    const body = `referenceNumber=${encodeURIComponent(refNum)}&task=${encodeURIComponent('GenerateNew')}`
    return this.auth.doBPMCalls(body).then(returned => returned as any);
  }

  generateCode(refNum: string, transactionNum : string) {
    console.log("Generating code for Reference Number: " + refNum);
    const param = "referenceNumber="+refNum+"&task=GenerateValidationCode"+"&numTransactionId=" + transactionNum;
    //return this.issuance
    return this.issuance.processBPM(param).then(returned => returned as any);
  }
  
  validateCode(refNum: string, validationCode: string, transactionNum : string) {
    console.log("validating code : " + validationCode);
    const param = "validationCode="+validationCode+"&referenceNumber=" + refNum +"&task=ValidateCode"+"&numTransactionId=" + transactionNum;
    return this.issuance.processBPM(param).then(returned => returned as any);
  }
  
  afterIssuance(stat: string, refNum: string) {
    console.log("validatedCodeAndBP parameters " + stat + " " + refNum);
    const param = "success="+stat+"&referenceNumber=" + refNum +"&task=validatedCodeAndBP";
    return this.issuance.processBPM(param).then(returned => returned as any);
  }
  
  afterPayment(stat: string, refNum: string, policyNum: string, transactionReferenceNumber: string, amountPaid: string) {
    console.log("ValidatePayment parameters " + stat + "  " + refNum + " " + policyNum);
    const param = "paymentStatus="+stat+"&referenceNumber=" + refNum +"&task=ValidatePayment"+"&numPoliza=" + policyNum +"&transactionReferenceNumber="
                  +transactionReferenceNumber+"&amountPaid="+amountPaid;
    return this.issuance.processBPM(param).then(returned => returned as any); 

  }
  
  /** Task: Exception - sends to policy central
    Input: 
 - referenceNumber: It's either quotation number or policy number depending on state
 - type: policy or reference
 - exception: error type
   Task: Exception

   */
  
  processPolicyCentral(refNum : string, tipo: string, error : string){
    console.log("processPolicyCentral parameters " + refNum + "  " + tipo + " " + error);
    const param = "type="+tipo+"&referenceNumber=" + refNum +"&task=Exception"+"&exception=" + error;
    return this.issuance.processBPM(param).then(returned => returned as any);
  }
  
  
  
}
