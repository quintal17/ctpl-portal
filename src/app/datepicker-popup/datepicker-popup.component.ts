import {Component, OnInit ,EventEmitter, Input, Output  } from '@angular/core';
import {NgbDateStruct,NgbInputDatepicker} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../_services/auth.service';

@Component({
  selector: 'ngbd-datepicker-popup',
  templateUrl: './datepicker-popup.component.html'
})
export class DatepickerPopupComponent {
  @Input() dateValue = "";
  @Output() dateOutput = new EventEmitter<NgbDateStruct>();
  @Input() dateType="";

  constructor(private auth:AuthService){
  	console.log(this.dateValue+" inception date preloaded.");

  }

  onDateSelect (date:NgbDateStruct){
    this.dateValue = date.month + '/' + date.day+ '/'+date.year;
    this.dateOutput.emit(date);
  }

  onClick(datePicker:NgbInputDatepicker){
    if(this.dateType =='Birthdate'){
      datePicker.minDate = this.getMinimumDate();
      datePicker.maxDate = this.getTodayDate();
    }
    datePicker.toggle();
  }

  getTodayDate(){
    var now = new Date();
    return {
              month:now.getMonth()+1,
              day:now.getDate(),
              year:now.getFullYear() - 18
           };  
  }

  getMinimumDate(){
    var now = new Date();
    return {
              month:now.getMonth()+1,
              day:now.getDate(),
              year:now.getFullYear() - 100  
           };  
  }
}